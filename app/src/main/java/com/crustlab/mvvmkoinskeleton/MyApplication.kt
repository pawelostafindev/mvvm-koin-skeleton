package com.crustlab.mvvmkoinskeleton

import android.app.Application
import com.crustlab.data.repository.di.repositoryModule
import com.crustlab.mvvmkoinskeleton.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class MyApplication : Application() {

    private val isDebug: Boolean
        get() = BuildConfig.DEBUG

    override fun onCreate() {
        super.onCreate()
        initializeTimber()
        initializeKoin()
    }

    private fun initializeTimber() {
        if (isDebug) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initializeKoin() {
        startKoin {
            if (isDebug) {
                // https://stackoverflow.com/a/63393508/4343449
                androidLogger(Level.INFO)
            }
            androidContext(this@MyApplication)
            modules(
                viewModelModule,
                repositoryModule
                // add other modules
            )
        }
    }

}