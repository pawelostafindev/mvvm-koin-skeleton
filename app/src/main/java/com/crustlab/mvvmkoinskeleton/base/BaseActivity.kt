package com.crustlab.mvvmkoinskeleton.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseActivity<ViewModel : BaseViewModel, Binding : ViewBinding> : AppCompatActivity() {

    protected abstract val viewModel: ViewModel
    protected abstract val bindingInflater: ActivityBindingInflater<Binding>

    private var _binding: Binding? = null
    protected val binding: Binding? get() = _binding
    protected val requireBinding: Binding get() = _binding!!

    protected val disposables = CompositeDisposable()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBindingAndContentView()
        setupView()
        observeViewModel()
        viewModel.initializeIfNeeded()
    }

    private fun setupBindingAndContentView() {
        _binding = bindingInflater.invoke(layoutInflater)
        val view = _binding!!.root
        setContentView(view)
    }

    @CallSuper
    protected open fun setupView() = Unit

    @CallSuper
    protected open fun observeViewModel() = Unit

    override fun onDestroy() {
        disposables.clear()
        _binding = null
        super.onDestroy()
    }

}

typealias ActivityBindingInflater<Binding> = (layoutInflater: LayoutInflater) -> Binding