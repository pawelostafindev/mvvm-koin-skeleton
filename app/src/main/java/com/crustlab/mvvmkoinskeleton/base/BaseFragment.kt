package com.crustlab.mvvmkoinskeleton.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import io.reactivex.rxjava3.disposables.CompositeDisposable

 abstract class BaseFragment<ViewModel : BaseViewModel, Binding : ViewBinding> : Fragment() {

    protected abstract val viewModel: ViewModel
    protected abstract val bindingInflater: FragmentBindingInflater<Binding>

    private var _binding: Binding? = null
    protected val binding: Binding? get() = _binding
    protected val requireBinding: Binding get() = _binding!!

    protected val disposables = CompositeDisposable()

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater.invoke(inflater, container, false)
        return _binding!!.root
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        observeViewModel()
        viewModel.initializeIfNeeded()
    }

    @CallSuper
    protected open fun setupView() = Unit

    @CallSuper
    protected open fun observeViewModel() = Unit

    @CallSuper
    override fun onDestroyView() {
        disposables.clear()
        _binding = null
        super.onDestroyView()
    }

}

typealias FragmentBindingInflater<Binding> =
            (layoutInflater: LayoutInflater, parent: ViewGroup?, attachToParent: Boolean) -> Binding