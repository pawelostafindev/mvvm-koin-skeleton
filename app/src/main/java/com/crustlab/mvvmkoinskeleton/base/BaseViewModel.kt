package com.crustlab.mvvmkoinskeleton.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.util.concurrent.atomic.AtomicBoolean

open class BaseViewModel : ViewModel() {

    private val isInitialized = AtomicBoolean(false)

    protected val disposables = CompositeDisposable()

    fun initializeIfNeeded() {
        if (!isInitialized.getAndSet(true)) {
            initialize()
        }
    }

    @CallSuper
    protected open fun initialize() = Unit

    @CallSuper
    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

}