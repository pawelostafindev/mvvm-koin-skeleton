package com.crustlab.mvvmkoinskeleton.di

import com.crustlab.mvvmkoinskeleton.main.MainViewModel
import com.crustlab.mvvmkoinskeleton.second.SecondViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel<MainViewModel> {
        MainViewModel(get())
    }

    viewModel<SecondViewModel> { (id: String) ->
        SecondViewModel(id)
    }

}