package com.crustlab.mvvmkoinskeleton.main

import android.widget.TextView
import androidx.core.view.setPadding
import com.crustlab.mvvmkoinskeleton.base.ActivityBindingInflater
import com.crustlab.mvvmkoinskeleton.base.BaseActivity
import com.crustlab.mvvmkoinskeleton.databinding.ActivityMainBinding
import com.crustlab.mvvmkoinskeleton.main.model.ListItem
import com.crustlab.mvvmkoinskeleton.second.SecondActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    override val viewModel: MainViewModel by viewModel()
    override val bindingInflater: ActivityBindingInflater<ActivityMainBinding>
        get() = ActivityMainBinding::inflate

    override fun setupView() {
        super.setupView()

    }

    override fun observeViewModel() {
        super.observeViewModel()

        viewModel.navigation.observe(this, ::handleNavigation)
        viewModel.listItems.observe(this, ::populateList)
    }

    private fun handleNavigation(navigation: MainViewModel.Navigation) {
        when (navigation) {
            is MainViewModel.Navigation.SecondActivity -> SecondActivity.start(this, navigation.id)
        }
    }

    private fun populateList(items: List<ListItem>) {
        requireBinding.linearLayout.removeAllViews()
        items.forEach { item ->
            when (item) {
                is ListItem.CustomerItem -> {
                    val textView = createTextView(item)
                    requireBinding.linearLayout.addView(textView)
                }
            }
        }
    }

    private fun createTextView(item: ListItem.CustomerItem): TextView {
        return TextView(this).apply {
            text = item.toString()
            setPadding(20)
            setOnClickListener {
                viewModel.goToCustomerDetails(id = item.id)
            }
        }
    }

}