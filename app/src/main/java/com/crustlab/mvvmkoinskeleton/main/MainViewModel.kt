package com.crustlab.mvvmkoinskeleton.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crustlab.domain.model.Customer
import com.crustlab.domain.repository.CustomerRepository
import com.crustlab.mvvmkoinskeleton.base.BaseViewModel
import com.crustlab.mvvmkoinskeleton.main.model.ListItem
import com.crustlab.mvvmkoinskeleton.util.livedata.LiveEvent
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy

class MainViewModel(
    private val customerRepository: CustomerRepository
) : BaseViewModel() {

    private val _listItems: MutableLiveData<List<ListItem>> = MutableLiveData()
    val listItems: LiveData<List<ListItem>> = _listItems

    private val _navigation: LiveEvent<Navigation> = LiveEvent()
    val navigation: LiveData<Navigation> = _navigation

    override fun initialize() {
        super.initialize()

        getCustomers()
    }

    private fun getCustomers() {
        customerRepository.getCustomers()
            .map { it.toListItems() }
            .subscribeBy(
                onSuccess = _listItems::postValue,
                onError = {}
            )
            .addTo(disposables)
    }

    fun goToCustomerDetails(id: String) {
        val navigation = Navigation.SecondActivity(id)
        _navigation.postValue(navigation)
    }

    private fun List<Customer>.toListItems(): List<ListItem> {
        return map {
            ListItem.CustomerItem(
                id = it.id,
                name = it.name,
                phoneNumber = it.phoneNumber
            )
        }
    }

    sealed class Navigation {
        data class SecondActivity(val id: String) : Navigation()
    }

}