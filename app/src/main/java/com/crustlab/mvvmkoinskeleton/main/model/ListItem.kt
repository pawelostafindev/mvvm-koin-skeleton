package com.crustlab.mvvmkoinskeleton.main.model

sealed class ListItem {

    data class CustomerItem(
        val id: String,
        val name: String,
        val phoneNumber: String
    ) : ListItem()

//    object LoadingItem : ListItem()

}