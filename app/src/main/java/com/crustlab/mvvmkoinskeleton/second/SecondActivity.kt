package com.crustlab.mvvmkoinskeleton.second

import android.content.Context
import android.content.Intent
import com.crustlab.mvvmkoinskeleton.base.ActivityBindingInflater
import com.crustlab.mvvmkoinskeleton.base.BaseActivity
import com.crustlab.mvvmkoinskeleton.databinding.ActivitySecondBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class SecondActivity : BaseActivity<SecondViewModel, ActivitySecondBinding>() {

    private val extraId: String by lazy {
        intent.extras!!.getString(EXTRA_ID) ?: error("EXTRA_ID required")
    }

    override val viewModel: SecondViewModel by viewModel(parameters = { parametersOf(extraId) })
    override val bindingInflater: ActivityBindingInflater<ActivitySecondBinding>
        get() = ActivitySecondBinding::inflate

    override fun setupView() {
        super.setupView()
    }

    override fun observeViewModel() {
        super.observeViewModel()

        viewModel.showId.observe(this) { requireBinding.textView.text = it }
    }

    companion object {

        private const val EXTRA_ID = "EXTRA_ID"

        fun start(context: Context, id: String) {
            val intent = Intent(context, SecondActivity::class.java).apply {
                putExtra(EXTRA_ID, id)
            }
            context.startActivity(intent)
        }

    }

}