package com.crustlab.mvvmkoinskeleton.second

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crustlab.mvvmkoinskeleton.base.BaseViewModel

class SecondViewModel(
    private val id: String
) : BaseViewModel() {

    private val _showId: MutableLiveData<String> = MutableLiveData()
    val showId: LiveData<String> = _showId

    override fun initialize() {
        super.initialize()

        emitId()
    }

    private fun emitId(){
        _showId.postValue(id)
    }

}