package com.crustlab.data.network.model

data class CustomerDTO(
    val id: String,
    val name: String,
    val number: String
)