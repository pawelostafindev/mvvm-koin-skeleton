package com.crustlab.data.repository

import com.crustlab.data.network.model.CustomerDTO
import com.crustlab.domain.model.Customer
import com.crustlab.domain.repository.CustomerRepository
import io.reactivex.rxjava3.core.Single

class CustomerRepositoryImpl : CustomerRepository {

    override fun getCustomers(): Single<List<Customer>> {
        return Single.just(getFakeApiData().toDomainCustomers())
    }

    private fun getFakeApiData() = listOf(
        CustomerDTO(
            id = "111111",
            name = "LEO",
            number = "2121312412342"
        ),
        CustomerDTO(
            id = "222222",
            name = "LEO2",
            number = "754756757"
        )
    )

    private fun List<CustomerDTO>.toDomainCustomers(): List<Customer> {
        return map {
            Customer(
                id = it.id,
                name = it.name,
                phoneNumber = it.number
            )
        }
    }

}