package com.crustlab.data.repository.di

import com.crustlab.data.repository.CustomerRepositoryImpl
import com.crustlab.domain.repository.CustomerRepository
import org.koin.dsl.module

val repositoryModule = module {

    single<CustomerRepository> {
        CustomerRepositoryImpl()
    }

}