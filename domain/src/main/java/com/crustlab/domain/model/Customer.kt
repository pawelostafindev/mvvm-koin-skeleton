package com.crustlab.domain.model

data class Customer(
    val id: String,
    val name: String,
    val phoneNumber: String
)