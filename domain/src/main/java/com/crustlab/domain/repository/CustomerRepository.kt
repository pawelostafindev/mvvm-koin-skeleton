package com.crustlab.domain.repository

import com.crustlab.domain.model.Customer
import io.reactivex.rxjava3.core.Single

interface CustomerRepository {

    fun getCustomers(): Single<List<Customer>>

}